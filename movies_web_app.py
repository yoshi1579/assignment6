import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode
from enum import Enum

application = app = Flask(__name__)
tableName = 'movies'


class Columns(Enum):
    __order__ = 'ID Year Title Director Actor Release_Date Rating'
    ID = 1
    Year = 2
    Title = 3
    Director = 4
    Actor = 5
    Release_Date = 6
    Rating = 7

class Stats(Enum):
    HIGH = 1
    LOW = 2

tableColumn = {}
for attribute in Columns:
    tableColumn[attribute] = attribute.name

def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE '+tableName+' (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, \
                                            Year INT, \
                                            Title TEXT, \
                                            Director VARCHAR(100), \
                                            Actor VARCHAR(100), \
                                            Release_Date VARCHAR(50), \
                                            Rating FLOAT, \
                                            PRIMARY KEY (ID))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def query_data(targetColumns=None, condition=''):

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    targetColumn = '*'
    if targetColumns:
        targetColumn = ', '.join(targetColumns)
    else:
        targetColumns = [col.name for col in Columns]
  
    searchQuery = "SELECT "+targetColumn+" FROM " + tableName + " "+  condition
    cur.execute(searchQuery)

    rows = [row for row in cur.fetchall()]
    
    return targetColumns, rows

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try: 
        cur = cnx.cursor()
        columns = ', '.join([key for key, value in request.form.items() if value])
        values = ', '.join(['"' + value + '"' for value in request.form.values() if value])
        insertQuery = "INSERT INTO " + tableName + " (" + columns + ") values (" + values + ")"
        cur.execute(insertQuery)
        cnx.commit()
        return render("Movie {} successfully inserted".format(request.form['title']))
    except Exception as e:
        return render("Movie {} could not be inserted - {}".format(request.form['title'], str(e)))


@app.route('/update_movie', methods=['POST'])
def update_movie():

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try: 
        titleToUpdate = request.form['title']
        condition = "WHERE Title='" + titleToUpdate +"'"
        _, data = query_data(['ID'], condition)
        if data:
            condition = "WHERE ID='" + str(data[0][0]) +"'"
            cur = cnx.cursor()
            columns = ', '.join([key + '="' + str(value) + '"' for key, value in request.form.items() if value])
            updateQuery = "UPDATE " + tableName + " SET " + columns + ' ' + condition
            cur.execute(updateQuery)
            cnx.commit()
            return render("Movie {} successfully updated".format(titleToUpdate))
        else:
            return render("Movie with {} does not exist".format(titleToUpdate))
    except Exception as e:
        return render("Movie {} could not be updated - {}".format(titleToUpdate, str(e)))


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try: 
        titleToDelete = request.form['delete_title']
        condition = "WHERE Title='" + titleToDelete +"'"
        _, data = query_data(['ID'], condition)
        if data:
            condition = "WHERE ID='" + str(data[0][0]) +"'"
            cur = cnx.cursor()
            deleteQuery = "DELETE FROM "+tableName+ " " + condition
            cur.execute(deleteQuery)
            cnx.commit()
            return render("Movie {} successfully deleted".format(titleToDelete))
        else:
            return render("Movie with {} does not exist".format(titleToDelete))
    except Exception as e:
        return render("Movie {} could not be deleted - {}".format(titleToDelete, str(e)))


@app.route('/search_movie', methods=['GET'])
def search_movie():
    conditionActor = request.args.get('search_actor')
    condition = 'WHERE Actor="' + conditionActor + '"'
    header, data = query_data(['Title', 'Year', 'Actor'], condition)

    if data:
        return render_template('index.html', message=formatOutput(data))
    else:
        return render_template('index.html', message="No movies found for actor {}".format(conditionActor))

def formatOutput(rows):
    output = ''
    for row in rows:
        output += ', '.join([str(data) for data in row]) + '<br>'
    return output

def print_stats(highOrLow):
    if highOrLow == Stats.HIGH:
        condition = "WHERE Rating = (SELECT MAX(Rating) FROM "+tableName+")"
    else:
        condition = "WHERE Rating = (SELECT MIN(Rating) FROM "+tableName+")"
    header, data = query_data(['Title', 'Year', 'Actor', 'Director', 'Rating'], condition)

    if data:
        return render_template('index.html', message=formatOutput(data))
    else:
        return render_template('index.html', message="No movies with rating")

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    return print_stats(Stats.HIGH)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    return print_stats(Stats.LOW)

@app.route("/")
def render(message=None):
    return render_template('index.html', message=message)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
